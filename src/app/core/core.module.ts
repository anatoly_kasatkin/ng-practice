import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SharedModule } from '@shared/shared.module';

const MODULES = [BrowserAnimationsModule, BrowserModule, SharedModule];

@NgModule({
  imports: [...MODULES],
  exports: [...MODULES],
})
export class CoreModule {}
