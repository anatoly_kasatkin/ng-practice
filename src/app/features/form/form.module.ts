import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SharedModule } from '@shared/shared.module';

import { FormRoutingModule } from './form-routing.module';
import { ReactiveFormExampleComponent } from './reactive-form-example/reactive-form-example.component';

@NgModule({
  declarations: [ReactiveFormExampleComponent],
  imports: [SharedModule, FormRoutingModule],
})
export class FormModule {}
