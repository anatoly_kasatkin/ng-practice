import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReactiveFormExampleComponent } from './reactive-form-example/reactive-form-example.component';

const ROUTES: Routes = [
  {
    path: ``,
    component: ReactiveFormExampleComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(ROUTES)],
  exports: [RouterModule],
})
export class FormRoutingModule {}
