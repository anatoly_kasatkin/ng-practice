import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';

import { merge, Observable } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';

import { Unsubscriptable } from '@utils';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: `app-reactive-form-example`,
  styleUrls: [`./reactive-form-example.component.scss`],
  templateUrl: `./reactive-form-example.component.html`,
})
export class ReactiveFormExampleComponent extends Unsubscriptable implements OnInit {
  reactiveForm: FormGroup;

  constructor(private cdRef: ChangeDetectorRef, private formBuilder: FormBuilder) {
    super();
  }

  ngOnInit(): void {
    this.reactiveForm = this.form;

    merge(this.changesFromA$, this.changesFromB$)
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => this.cdRef.detectChanges());
  }

  erase(): void {
    this.reactiveForm.setValue({ A: ``, B: `` }, { onlySelf: true, emitEvent: false });
    this.cdRef.detectChanges();
  }

  private get form(): FormGroup {
    return this.formBuilder.group({
      A: [``],
      B: [``],
    });
  }

  private get A(): AbstractControl {
    return this.reactiveForm.controls[`A`];
  }

  private get B(): AbstractControl {
    return this.reactiveForm.controls[`B`];
  }

  private get changesFromA$(): Observable<any> {
    return this.A.valueChanges.pipe(
      tap((value) => this.B.patchValue(`${this.B.value}${value}`, { onlySelf: true, emitEvent: false })),
    );
  }

  private get changesFromB$(): Observable<any> {
    return this.B.valueChanges.pipe(
      tap((value) => this.A.patchValue(`${this.A.value}${value}`, { onlySelf: true, emitEvent: false })),
    );
  }
}
