import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: `app-home`,
  styleUrls: [`./home.component.scss`],
  templateUrl: `./home.component.html`,
})
export class HomeComponent {}
