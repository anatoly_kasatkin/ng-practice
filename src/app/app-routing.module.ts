import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const ROUTES: Routes = [
  {
    path: ``,
    children: [
      {
        loadChildren: () => import(`app/features/form/form.module`).then((m) => m.FormModule),
        path: `forms`,
        pathMatch: `full`,
      },
      {
        loadChildren: () => import(`app/features/home/home.module`).then((m) => m.HomeModule),
        path: `home`,
        pathMatch: `full`,
      },
      {
        path: `**`,
        redirectTo: `home`,
      },
    ],
  },
  {
    path: `**`,
    redirectTo: ``,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(ROUTES, { useHash: false })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
