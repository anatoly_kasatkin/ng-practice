import { OnDestroy } from '@angular/core';

import { Subject } from 'rxjs';

export abstract class Unsubscriptable implements OnDestroy {
  protected readonly destroy$ = new Subject<void>();

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
