/**
 * Polyfill all ES features.
 */
import 'core-js';

import 'formdata-polyfill/formdata.min.js';
import 'svgxuse/svgxuse.js';

/**
 * IE 10-11 requires the following for NgClass support on SVG elements.
 */
import 'classlist.js';

/**
 * Web Animations `@angular/platform-browser/animations`.
 * Only required if AnimationBuilder is used within the application and using IE/Edge or Safari.
 * Standard animation support in Angular DOES NOT require any polyfills (as of Angular 6.0).
 */
import 'web-animations-js';

/**
 * Zone JS is required by default for Angular itself. Included with Angular CLI.
 */

import 'zone.js/dist/zone';
